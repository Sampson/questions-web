# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20150123_0359'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Category',
            new_name='QuestionCategory',
        ),
    ]
