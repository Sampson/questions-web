import datetime

from django.db import models
from django.utils import timezone

class QuestionCategory(models.Model):
    category_name = models.CharField(max_length=100)
    def __unicode__(self):
        return self.category_name

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.choice_text

class User(models.Model):
    name = models.CharField(max_length=50)
    answers_earned = models.IntegerField(default=0)

class Answer(models.Model):
    choice = models.ForeignKey(Choice)
    user = models.ForeignKey(User)
